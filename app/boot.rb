require_relative "kyouko"
require_relative "konachan"

TOMATOES = {
  japanese: "トマト",
  korean:   "토마토",
  russian:  "помидор",
  english:  "tomato",
  german:   "Tomate",
  dutch:    "tomaat",
  italian:  "pomodoro",
  greek:    "ντομάτα",
  serbian:  "парадајз"
}.freeze

# The most important function
def rand_tomato
  TOMATOES.values.sample
end

Kyouko.new do |bot|
  bot.ready do |event|
    event.bot.game = "トマト"
  end

  bot.mention do |event|
    next rand_tomato if event.content.empty?

    tomatoes =
      TOMATOES.select do |key, _value|
        Regexp.new(key.to_s, Regexp::IGNORECASE) =~ event.content
      end

    event << tomatoes.values.join("\n")
  end

  bot.owner_command :tomato do |_event, *args|
    puts args
    next rand_tomato if args.empty?
    TOMATOES.select { |k, _v| k.match?(args.first) }.values.fetch(0) { rand_tomato }
  end

  bot.owner_command %i[yuru_yuri yuruyuri yryr] do |_event|
    Konachan.message("yuru_yuri")
  end

  bot.owner_command %i[konachan kc] do |_event, *query|
    Konachan.message(query.join(" "))
  end

  bot.owner_command %i[nsfw] do |_event, *query|
    Konachan.message(query.join(" "), safe: false)
  end

  # rubocop:disable Security/Eval
  bot.owner_command %i[ruby eval rb] do |_event, *code|
    result = eval(code.join(" "))
    "```ruby\n#{result}\n```"
  end
end
