require "json"
require "rest-client"

module Konachan
  module_function

  def search(tags, safe: "eq")
    tag_string = tags.downcase.gsub(/, ?/, "+").gsub(/ +/, "_")
    url = "https://konachan.net/post.json"
    options = { content_type: :json, accept: :json, params: { tags: tag_string } }
    RestClient.get(url, options) do |response|
      json = JSON.parse(response.body)
      json.reject! { |x| safe.include?(x["rating"]) } if safe
      json.map { |e| image_format(e) }
    end
  end

  def image_format(json)
    {
      sample:  "https:#{json['sample_url']}",
      preview: "https:#{json['preview_url']}",
      file:    "https:#{json['file_url']}",
      source:  json["source"]
    }
  end

  def message(tags, **options)
    image = search(tags, options).sample
    return if image.nil?
    file = "`Image:` <#{image[:file]}>" unless image[:file].empty?
    "#{file}\n#{image[:preview]}"
  end
end
