require "discordrb"
require "dotenv/load"
require "erb"
require "yaml"
require "logger"
require "active_support/core_ext/hash/deep_merge"
require "active_support/core_ext/hash/keys"

class Kyouko < Discordrb::Commands::CommandBot
  attr_reader :log

  def initialize
    @app_root = Pathname.new(__dir__).join("..")
    @config   = {}
    @log      = Logger.new(STDOUT)

    trap("TERM") { exit 0 }

    load_config

    super @config[:bot]
    yield self

    startup_log
    run

    exit 0
  end

  def owner?(user)
    return true if @config[:owner_id] == user.id
    @log.error("#{user.distinct} (#{user.id}) tried to use a owner-only command")
    false
  end

  def owner_command(name, **options)
    command(name, options) do |event, *args|
      next unless owner?(event.author)
      yield event, *args
    end
  end

private

  def startup_log
    @log.info("Using prefix: #{@config.dig(:bot, :prefix)}")
    @log.info("Owner id:     #{@config[:owner_id]}")
  end

  def load_config
    @app_root.join("config").each_child do |file|
      erb = ERB.new(file.read).result
      yml = YAML.safe_load(erb).deep_symbolize_keys
      @config.deep_merge!(yml)
    end
    @config.freeze
  end
end
