FROM "ruby:2.4-alpine" 

RUN apk --no-cache add --update \
  build-base \
  libffi-dev

ENV APP_ROOT /app
RUN mkdir -p $APP_ROOT
WORKDIR $APP_ROOT

COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . .

ENTRYPOINT ["bundle", "exec"]
CMD ["rake", "bot"]